#tls-dynamic

Example of conditionally rendering dynamic blocks

1. First create a private key
```
openssl req -x509 -newkey rsa:4096 -nodes -keyout key.pem -out cert.pem -days 10000
```
1. Run terraform apply
1. Display certs using openssl
```
openssl x509 -noout -text -in tfcert.pem
```
