# Renders the subject block
module "tls" {
  source = "./tls-cert"

  # Set required values to create a subject block
  subject_common_name = "amazonaws.com"
  subject_organization = "AWS"
  subject_organizational_unit = "Solution Architecture"

  # Set an optional variable
  subject_country = "USA"
}

# Does not render the subject block
module "tls2" {
  source = "./tls-cert"
  
  # Setting only one variable does not render the subject block
  subject_common_name = "amazon.com"
}
