output "cert" {
  value = tls_self_signed_cert.example.cert_pem
}

output "locals" {
  value = local.create_subject
}
