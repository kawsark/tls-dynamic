variable "subject_common_name" {
    default = ""
}

variable "subject_organization" {
    default = ""
}

variable "subject_organizational_unit" {
    default = ""
}

variable "subject_province" {
    default = ""
}

variable "subject_country" {
    default = ""
}
