locals {
  create_subject = (length(var.subject_common_name) > 0 && length(var.subject_organization) > 0 && length(var.subject_organizational_unit) > 0)
}

resource "tls_self_signed_cert" "example" {
  private_key_pem = file("key.pem")

  dynamic "subject" {
    for_each = local.create_subject == true ? [1] : []
    content {

      # Required 
      common_name = var.subject_common_name
      organization = var.subject_organization
      organizational_unit = var.subject_organizational_unit

      # Optional
      country = length(var.subject_country) > 0 ? var.subject_country : null
      province = length(var.subject_province) > 0 ? var.subject_province : null

    }

  }

  validity_period_hours = 12

  allowed_uses = [
    "key_encipherment",
    "digital_signature",
    "server_auth",
  ]
}

resource "local_file" "cert" {
    content  = tls_self_signed_cert.example.cert_pem
    filename = "tfcert-${var.subject_common_name}.pem"
}